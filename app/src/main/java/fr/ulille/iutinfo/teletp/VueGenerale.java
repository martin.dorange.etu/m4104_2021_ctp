package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1

    private String salle, poste,distanciel;
    //private final String DISTANCIEL;

    // TODO Q2.c

    SuiviViewModel model;
    Spinner spPoste;
    Spinner spSalle;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1

        this.distanciel = getResources().getStringArray(R.array.list_salles)[0];
        this.poste="";
        this.salle=this.distanciel;

        // TODO Q2.c

        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);

        // TODO Q4

        spSalle = view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(super.getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapter);
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = parent.getItemAtPosition(position)+"";
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spPoste = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterP = ArrayAdapter.createFromResource(super.getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterP);
        spPoste.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = parent.getItemAtPosition(position)+"";
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });;

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3

            TextView login = (TextView) view.findViewById(R.id.tvLogin);
            model.setUsername(login.getText()+"");

            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b

        update();

        // TODO Q9


    }


    // TODO Q5.a

    private void update(){
        if (salle.equals(distanciel)){
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
        }else{
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
        }
        model.setLocalisation(salle+" : "+poste);
    }



    // TODO Q9
}